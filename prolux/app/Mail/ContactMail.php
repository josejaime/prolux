<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->email = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //contacto@prolux.mx
        //GmQtmRmw)rLa
        return $this->subject('Contacto PROLUX')
                    ->from('contacto@prolux.mx')
                    ->to('agodinez@conjunto.mx')
                    ->bcc('jaime@secuenciaestrategica.com')
                    ->bcc('adrian.gutierrez@prolux.mx')
                    ->bcc('guillermo.vega@prolux.mx')
                    ->view('email.contact');
                    //->to($this->email->email)
    }
}
