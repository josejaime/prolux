@extends('layout.main')

@section('title', 'Home')

@section('content')
    <!-- ======================================================= -->
    <div id="home-slide">
        {{-- <img src="{{ asset('images/cpef.png') }}" alt="" class="home-cpef" style="position:absolute;"> --}}
        <div id="owl-carousel2" class="owl-carousel left-side">
            {{-- <div class="column left-side" style="background-image: url({{ asset('images/PROLUX_Web_ImagenPrincipal_1280x1280px_ConAdornos_.png') }})"> --}}
            {{-- <img src="{{ asset('images/PROLUX_Web_LineasAmarillas_EnMedio_520px_.png') }}" alt="" class="home-lines"> --}}
            <div class="column" style="background-image: url({{ asset('images/ventas_1.jpg') }})">
                {{-- <img src="{{ asset('images/PROLUX_Web_ImagenPrincipal_1280x1280px_ConAdornos_.png') }}" alt=""> --}}
            </div>
            <div class="column" style="background-image: url({{ asset('images/ventas_2.jpg') }})">
            </div>
            <div class="column" style="background-image: url({{ asset('images/ventas_3.jpg') }})">
            </div>
            <div class="column" style="background-image: url({{ asset('images/ventas_4.jpg') }})">
            </div>
            {{-- <div class="column" style="background-image: url({{ asset('images/comments_1.png') }})">
            </div> --}}
        </div>
        <div class="column right-side">
            {{-- <img src="{{ asset('images/PROLUX_Web_CirculoBlanco_520x520px_.png') }}" alt="" class="home-circle"> --}}
            <img src="{{ asset('images/medio_circulo_blanco.png') }}" alt="" class="home-circle">
                <div data-aos="fade-up">
                    <form class="ui form" id="main-form" action="{{ route('send-mail') }}" method="post">
                        @csrf
                        <h1 class="ui dividing header" style="margin-top: 5px;">¿Quieres conocer cuánto puedes ahorrar en consumo de energía?</h1>
                        <legend>
                            <p>
                                Con los paneles solares PROLUX tienes garantizada una reducción de hasta el 99% del costo actual de tu recibo de luz. Solo comparte con nosotros información acerca de tu consumo actual y te diremos hasta cuánto puedes ahorrar.
                            </p>
                        </legend>
                        <div class="field">
                            <input type="text" name="name" placeholder="Nombre completo">
                        </div>
                        <div class="field">
                            <input type="text" placeholder="Correo" name="email">
                        </div>
                        <div class="field">
                            <input type="text" placeholder="Teléfono" name="phone">
                        </div>
                        <div class="field">
                            {{-- <label>State</label> --}}
                            <select class="ui fluid dropdown" name="consumo">
                                <option value="Menos de $5000">¿Cuánto consumes actualmente?</option>
                                <option value="Menos de $5000">*Menos de $5000</option>
                                <option value="$5000-$10000">*$5000-$10000</option>
                                <option value="$10000-$15000">*$10000-$15000</option>
                                <option value="$15000-$25000">*$15000-$25000</option>
                                <option value="$25000-$30000">*$25000-$30000</option>
                            </select>
                        </div>
                        {{-- <div class="ui icon field pop" data-content="Add users to your feed">
                            <input type="text" name="servicio" placeholder="Número de servicio CFE">
                            <i class="search icon"></i>
                        </div> --}}
                        <div class="ui icon input field" style="display: flex; flex-flow: row;">
                            <input type="text" placeholder="Número de servicio" class="" style="flex: 10;" name="number">
                            {{-- <i class="search icon"></i> --}}
                            <div class="ui icon button" data-tooltip="Da click para más información" data-inverted="" data-variation="basic" data-position="left center" style="flex:1;" onclick="openRecipt()">
                                <i class="question icon"></i>
                            </div>
                        </div>
                        <div class="ui button" tabindex="0" id="button-main">¡Comienza a ahorrar con paneles solares!</div>
                        <div class="main-error">
                            <i></i>
                        </div>
                    </form>
                </div>
            </div>
    </div>
    <div class="relative">
        <img src="{{ asset('images/PROLUX_Web_LineasAmarillas_EnMedio_520px_.png') }}" alt="" class="sides-lines">
        <img src="{{ asset('images/PROLUX_Web_520px_RectanguloCirculosGrises_.png') }}" alt="" class="sides-squares">

        <div class="shrinked" id="shrinked">
            <div class="topper">
                <div class="gray">
                    <div class="title">
                        <h2>Paneles Solares la mejor inversión</h2>
                    </div>
                    <div>
                        <p>Puedes tener tus paneles solares PROLUX con un crédito el cuál el monto de tus mensualidades sea equivalente a lo que pagas actualmente en energía.</p>
                    </div>
                    <div>
                        <p>Al final del crédito simplemente dejas de pagar y comienzas a recuperar tu inversión y generar importantes ahorros.</p>
                    </div>
                    <div>
                        <p>Un sistema de paneles solares es una inversión superior a la de la mayoría de las inversiones actuales, llegando hasta niveles del 30% de rendimiento.</p>
                    </div>
                </div>
                <div class="white" data-aos="fade-down">
                    <h1>En Prolux te garantizamos ahorro en cada recibo de luz para tu casa o negocio</h1>
                    <div class="icons">
                        <div class="icon" data-aos="flip-left">
                            <img class="lazy" data-src="{{ asset('images/PROLUX_Web_1024x720px_Iconos_1_.png') }}" alt="">
                            <p>
                                Ahorro de hasta un 99% en tu recibo de luz. ¡No pagues más!
                            </p>
                        </div>
                        <div class="icon" data-aos="flip-left" data-aos-delay="300">
                            <img class="lazy" data-src="{{ asset('images/PROLUX_Web_1024x720px_Iconos_2_.png') }}" alt="">
                            <p>
                                Equipos de calidad internacional con garantías y respaldos.
                            </p>
                        </div>
                        <div class="icon" data-aos="flip-left" data-aos-delay="600">
                            <img class="lazy" data-src="{{ asset('images/PROLUX_Web_1024x720px_Iconos_3_.png') }}" alt="">
                            <p>
                                Instalaciones limpias y no invasivas.
                            </p>
                        </div>
                        <div class="icon" data-aos="flip-left" data-aos-delay="900">
                            <img class="lazy" data-src="{{ asset('images/PROLUX_Web_1024x720px_Iconos_4_.png') }}" alt="">
                            <p>
                                Monitoreo de tu consumo de energía vía App.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="green" data-aos="fade-down">
                    <div class="icons">
                        <div class="icon" data-aos="flip-right" data-aos-delay="900">
                            <img class="lazy" data-src="{{ asset('images/PROLUX_Web_1024x720px_Iconos_5_.png') }}" alt="">
                            <p>
                                Reduce la dependencia energética ante cambios de tarifas de la CFE.
                            </p>
                        </div>
                        <div class="icon" data-aos="flip-right" data-aos-delay="600">
                            <img class="lazy" data-src="{{ asset('images/PROLUX_Web_1024x720px_Iconos_6_.png') }}" alt="">
                            <p>
                                Consume energía verde e inagotable, ayudando al planeta. Apoya a combatir el cambio climático.
                            </p>
                        </div>
                        <div class="icon" data-aos="flip-right" data-aos-delay="300">
                            <img class="lazy" data-src="{{ asset('images/PROLUX_Web_1024x720px_Iconos_7_.png') }}" alt="">
                            <p>
                                ¡Quédate con ellos! si te cambias de domicilio tus paneles se mueven contigo.
                            </p>
                        </div>
                        <div class="icon" data-aos="flip-right">
                            <img class="lazy" data-src="{{ asset('images/PROLUX_Web_1024x720px_Iconos_8_.png') }}" alt="">
                            <p>
                                Aprovecha la energía del sol de México, que tiene un excelente potencial de captación de energía social.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="white" data-aos="fade-down">
                    <h1>En Prolux contamos con la certificación CPEF</h1>
                    <div class="icons">
                        <div data-aos="flip-left" style="text-align:center; width:100%;">
                            <img src="{{ asset('images/PROLUX_Logo_1280x_1_.png') }}" alt="" style="width:125px; margin: 0 15px; vertical-align: middle;">
                            {{-- <img src="{{ asset('images/cpef.png') }}" alt="" style="width:125px; margin: 0 15px; vertical-align: middle;"> --}}
                            <a href="https://cpef.org.mx/" target="_blank">
                                <img src="{{ asset('images/certificacion_prolux_1.png') }}" alt="" style="width:205px; margin: 0 15px; vertical-align: middle;">
                            </a>
                        </div>
                    </div>
                    <h2> Certificado como profesional en energía fotovoltaica</h2>
                </div>
            </div>
        </div>
    </div>
    <div id="thirds">
        <div class="third" data-aos="fade-up" style="background-image: url('{{ asset('images/seccion_1.jpg') }}')">
            <span></span>
            <div class="content">
                <h2>Residencia</h2>
                <a href="#!" onclick="openFotos(1)">Ver Más</a>
            </div>
        </div>
        <div class="third" data-aos="fade-up" data-aos-delay="300" style="background-image: url('{{ asset('images/seccion_2.jpg') }}')">
            <span></span>
            <div class="content">
                <h2>Industrial</h2>
                <a href="#!" onclick="openFotos(2)">Ver Más</a>
            </div>
        </div>
        <div class="third" data-aos="fade-up" data-aos-delay="600" style="background-image: url('{{ asset('images/seccion_3.jpg') }}')">
            <span></span>
            <div class="content">
                <h2>Negocio</h2>
                <a href="#!" onclick="openFotos(3)">Ver Más</a>
            </div>
        </div>
    </div>
    <div id="works" data-aos="fade-down">
        <img src="{{ asset('images/PROLUX_Web_Linas_azules_520px_.png') }}" alt="" class="blue-lines">
        <img src="{{ asset('images/PROLUX_Web_RectanguloCirculosAzules_520px_.png') }}" alt="" class="blue-squares">

        <div class="left-side">
            <div class="title">
                <h2>¿Cómo funcionan los paneles solares de prolux?</h2>
            </div>
            <div class="text">
                <p>
                    Los paneles solares tienen un funcionamiento muy simple,
                    que está basado en que la energía del sol se envía a la red
                    de CFE, que nos descuenta lo que enviamos de nuestro consumo.
                    Te lo explicamos en simples pasos:
                </p>
                <p>
                    <big><b>Paso 1:</b></big> Los rayos del sol llegan a los paneles solares
                    los cuales la convierten en corriente continua y la cual es enviada
                    al inversor.
                </p>
                <p>
                    <big><b>Paso 2:</b></big> El inversor convierte la energía solar en
                    corriente alterna que es la que normalmente usamos día a día
                    y se envía esa corriente a un medidor bidireccional (que puede
                    recibir y enviar energía)
                </p>
                <p>
                    <big><b>Paso 3:</b></big> La energía remanente se envía a la red de CFE,
                    la cual se descuenta del consumo que tuvimos, lo cual puede
                    generar incluso saldos a favor del usuario.
                </p>
            </div>
        </div>
        <div class="right-side" data-aos="fade-up" data-aos-delay="500">
            <img src="{{ asset('images/PROLUX_Web_ComoFunciona_1280x1280px_.png') }}" alt="">
        </div>
    </div>
    <div id="future">
        <div class="side-left">
            <div class="content" data-aos="fade-right">
                <h2>Pensamos en tu futuro</h2>
                <p>Tener paneles solares no solamente significa una eficiente forma de ahorrar dinero, también es una herencia que dejas al planeta y a los tuyos.</p>

                <p>Las garantías de los paneles solares PROLUX son las más amplias del mercado, debido a la alta calidad de los componentes e instalación, de forma que podrían llegar hasta 100 años de durabilidad.</p>

                <p>Con el uso de paneles solares ayudas a nuestro planeta a evitar el cambio climático al inhibir el uso de combustibles fósiles para generar energía. </p>

                <p>Pensando en tu futuro, en PROLUX además contamos con un sistema de monitoreo permanente de tus paneles, de forma que verificamos su estado al momento para que siempre estén trabajando con un rendimiento óptimo.</p>

            </div>
        </div>
        <div class="side-right">
            <img data-aos="fade-up" src="{{ asset('images/futuro_1.jpg') }}" alt="">
            <img data-aos="fade-up" data-aos-delay="300" src="{{ asset('images/futuro_2.jpg') }}" alt="">
            <img data-aos="fade-up" data-aos-delay="600" src="{{ asset('images/futuro_3.jpg') }}" alt="">
        </div>
    </div>
    <div id="blue" data-aos="fade-right">
        {{-- <img src="{{ asset('images/PROLUX_Web_CirculoAmarillo_1024x1024px_.png') }}" alt="" class="side-circle"> --}}
        <img src="{{ asset('images/medio_circulo.png') }}" alt="" class="side-circle">
        <div class="content">
            <div class="left-side">
                <h3>En PROLUX nos encargamos además de TODOS los trámites que hay que hacer ante CFE para que tengas tus paneles funcionando. Déjanos tu número de servicio de CFE y comienza a ahorrar. </h3>
            </div>
            <div class="right-side">
                <form class="ui form" method="post">
                    @csrf
                    <div class="ui icon input field" style="display: flex; flex-flow: row;">
                        <input type="text" placeholder="Número de servicio" class="" style="flex: 10;" name="number" id="serviceNumberBottom">
                        <div class="ui icon button" data-tooltip="Da click para más información" data-inverted="" data-variation="basic" data-position="left center" style="flex:1;" onclick="openRecipt()">
                            <i class="question icon"></i>
                        </div>
                    </div>
                    <div class="ui button" tabindex="0" onclick="openModal()">
                        ¡Comienza a ahorrar con paneles solares!
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- ======================================================* -->
    <div id="modalForm" class="ui modal">
        <div class="header">¡Cotiza ahora!</div>
        <div class="content">
            <form class="ui form" id="modal-form" action="{{ route('send-mail') }}" method="post">
                @csrf
                <div class="field">
                    <input type="text" name="name" placeholder="Nombre completo">
                </div>
                <div class="field">
                    <input type="text" placeholder="Correo" name="email">
                </div>
                <div class="field">
                    <input type="text" placeholder="Teléfono" name="phone">
                </div>
                <div class="field">
                    <select class="ui fluid dropdown" name="consumo">
                        <option value="">¿Cuánto consumes actualmente?</option>
                        <option value="Menos de $5000">*Menos de $5000</option>
                        <option value="$5000-$10000">*$5000-$10000</option>
                        <option value="$10000-$15000">*$10000-$15000</option>
                        <option value="$15000-$25000">*$15000-$25000</option>
                        <option value="$25000-$30000">*$25000-$30000</option>
                    </select>
                </div>
                <div class="ui icon input field" style="display: flex; flex-flow: row;">
                    <input type="text" placeholder="Número de servicio" class="" style="flex: 10;" id="serviceNumberModal" name="number">
                    {{-- <i class="search icon"></i> --}}
                    <div class="ui icon button" style="flex:1;" onclick="openRecipt()">
                        <i class="question icon"></i>
                    </div>
                </div>
                <div class="ui button" tabindex="0" id="modal-button">
                    ¡Comienza a ahorrar con paneles solares!
                </div>
                <div class="modal-error">
                    <i></i>
                </div>
            </form>
        </div>
    </div>
    <!-- ======================================================* -->
    <div id="modalFotos" class="ui modal">
        <div class="header" onclick="openModal()">¡Cotiza ahora!</div>
        <div class="content">
            <div id="fotos" class="owl-carousel">
                <div class="block">
                    {{-- <div class="image" style="background-image: url({{ asset('images/comments_1.png') }})">
                    </div> --}}
                    <img class="lazy" data-src="{{ asset('images/comments_1.jpg') }}">
                    {{-- <div class="text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <span class="line"></span>
                        <h3>Lorem Ipsum dolor</h3>
                    </div> --}}
                </div>
                <div class="block">
                    {{-- <div class="image" style="background-image: url({{ asset('images/comments_2.png') }})">
                    </div> --}}
                    <img class="lazy" data-src="{{ asset('images/comments_2.jpg') }}">
                    {{-- <div class="text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <span class="line"></span>
                        <h3>Lorem Ipsum dolor</h3>
                    </div> --}}
                </div>
                <div class="block">
                    {{-- <div class="image" style="background-image: url({{ asset('images/comments_3.png') }})">
                    </div> --}}
                    <img class="lazy" data-src="{{ asset('images/comments_3.jpg') }}">
                    {{-- <div class="text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <span class="line"></span>
                        <h3>Lorem Ipsum dolor</h3>
                    </div> --}}
                </div>
                <div class="block">
                    <img class="lazy" data-src="{{ asset('images/comments_4.jpg') }}">
                </div>
            </div>
        </div>
    </div>
    <!-- ======================================================* -->
    <div id="modalSent" class="ui modal">
        <div class="header" style="text-align:center;">¡Gracias por contactarnos!</div>
        <div class="content">
            <h2 style="text-align:center;">Recibimos tu correo y nos pondremos en contacto contigo lo antes posible</h2>
        </div>
    </div>
    <!-- ======================================================* -->
    <div id="modalRecipt" class="ui modal">
        <div class="header" style="text-align:center;">Tu número de servicio</div>
        <div class="content">
            {{-- <h2 style="text-align:center;">Recibimos tu correo y nos pondremos en contacto contigo lo antes posible</h2> --}}
            <img src="{{ asset('images/PROLUX_Foto_NumeroDeServicio_800x600px_.png') }}" alt="">
        </div>
    </div>
@endsection

@push('styles')
    {{-- <link rel="stylesheet" href="{{ asset('css/landing.min.css') }}"> --}}
@endpush

@push('scripts')
    <script>
        $(function() {
           $('.lazy').Lazy();
        });
    </script>
    <script>
        @if (session('status'))
            $('#modalSent.ui.modal').modal('show');
            gtag('event', 'conversion', {'send_to': 'AW-696438563/TcTSCILpiuIBEKOei8wC'});
        @endif
        function openModal() {
            $('#modalForm.ui.modal').modal('show');

            if ($('#serviceNumberBottom').val()) {
                $("#serviceNumberModal").val($('#serviceNumberBottom').val())
            };
        }
        function openFotos( index ) {
            $('#modalFotos.ui.modal').modal('show');
            return index;
        }
        function openRecipt(){
            $('#modalRecipt.ui.modal').modal('show');
            return;
        }
        jQuery( document ).ready( function() {
            $('#owl-carousel2.owl-carousel').owlCarousel({
                loop:true,
                margin: 0,
                autoplay: true,
                dots: false,
                nav:true,
                lazyLoad: true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
            });
        });
        jQuery(function($) {

            /*$('#modalForm.ui.modal').modal({ onHide: function() {
                $('.modal-error i').html('');
                $('#modal-form input[type="text"]').parent().removeClass('error');
            }, });*/
            //$('#modalFotos.ui.modal').modal();


            $('#blocks.owl-carousel').owlCarousel({
                loop:true,
                margin:0,
                nav:false,
                dots: false,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:2
                    },
                    1000:{
                        items:2
                    }
                }
            });
            $('#fotos.owl-carousel').owlCarousel({
                loop:true,
                margin:5,
                nav:true,
                dots: false,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:2
                    },
                    1000:{
                        items:3
                    }
                }
            });
            $('.pop')
              .popup()
            ;

            $('#button-main').on('click', function(){
                var ok = true;
                $('.main-error i').html('');

                $('#main-form input[type="text"]').parent().removeClass('error');
                $('#main-form input[type="text"]').each( function() {
                    if (! $(this).val() ) {
                        ok = false;
                        $(this).parent().addClass('error');
                    }
                    if (ok === false) {
                        $('.main-error i').html('* Primero debes completar todos los campos');
                        return;
                    }
                });
                if( ok ) {
                    //console.log("Enviar");
                    fbq('track', 'Lead');
                    gtag('event', 'clickFormulario', {
                      'event_category': 'Contacto',
                      'event_label': 'slider',
                    });
                    $('#main-form').submit();
                }
            });
            $('#modal-button').on('click', function(){
                var ok = true;
                $('.modal-error i').html('');

                $('#modal-form input[type="text"]').parent().removeClass('error');
                $('#modal-form input[type="text"]').each( function() {
                    if (! $(this).val() ) {
                        ok = false;
                        $(this).parent().addClass('error');
                    }
                    if (ok === false) {
                        $('.modal-error i').html('* Primero debes completar todos los campos');
                    }
                });
                if( ok ) {
                    //console.log("ENVIAR");
                    fbq('track', 'Lead');
                    gtag('event', 'clickPopup', {
                      'event_category': 'Contacto',
                      'event_label': 'popup',
                    });
                    $('#modal-form').submit();
                }
            });
        });
    </script>
    <script>
        AOS.init();
    </script>
@endpush
