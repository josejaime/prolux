<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Contacto Prolux</title>
        <style media="screen">
            table {
                width: 80%;
                margin: 0 auto;
                background: transparent;
            }
            thead {
                background-color: #0a4da7;
                color: white;
            }
            thead th {
                padding: 20px 15px;
            }
            tbody tr.names td {
                color: white;
                text-align: center;
                background: #facc13;
            }
            tbody td {
                padding: 10px 15px;
                text-align: center;
            }
            .header td {
                padding: 15px;
                color: white;
                background-color: #0a4da7;
            }
            h3 {
                margin: 0;
                text-transform: uppercase;
            }
            .info {
                margin-bottom: 15px;
            }
        </style>
    </head>
    <body>

        <p><h4>Datos generados en prolux.mx</h4></p>
        <p>Correo generado automaticamente por <a target="_blank" href="http://secuenciaestrategica.com">secuenciaestrategica.com</a></p>
        
        <table>
            <thead>
                <tr>
                    <th colspan="15"><h3>PROLUX</h3></th>
                </tr>
            </thead>
            <tbody>
                <tr class="names">
                    <td>Nombre</td>
                    <td>Email</td>
                    <td>Teléfono</td>
                </tr>
                <tr class="info">
                    {{-- <td>{{ "this->name" }}</td>
                    <td>{{ "this->email" }}</td>
                    <td>{{ "this->phone" }}</td> --}}
                    <td>{{ $email->name }}</td>
                    <td>{{ $email->email }}</td>
                    <td>{{ $email->phone }}</td>
                </tr>
                <tr>
                    <td><h5></h5></td>
                </tr>
                <tr class="header">
                    <td colspan="10"><h3>Datos de consumo</h3></td>
                </tr>
                <tr class="names">
                    <td>Consumo</td>
                    <td colspan="2">Número de servicio</td>
                </tr>
                <tr class="info">
                    <td>{{ $email->consumo }}</td>
                    <td colspan="2">{{ $email->number }}</td>
                    {{-- <td>{{ "this->consumo" }}</td>
                    <td colspan="2">{{ "this->number" }}</td> --}}
                </tr>
            </tbody>
        </table>

    </body>
</html>
