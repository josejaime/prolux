<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Mobile First :: Disable the zooming capabilities in mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Prolux | @yield('title')</title>

    <meta name="description" content="">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('lib/owl/dist/assets/owl.carousel.min.css?v=1.0.0') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('lib/owl/dist/assets/owl.theme.default.min.css?v=1.0.0') }}">
    {{-- <link rel="preload" href="{{ asset('lib/owl/dist/assets/owl.carousel.min.css?v=1.0.0') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('lib/owl/dist/assets/owl.carousel.min.css?v=1.0.0') }}"></noscript> --}}

    <link rel="stylesheet" type="text/css" href="{{ asset('lib/semantic/semantic.min.css') }}">
    {{-- <link rel="preload" href="{{ asset('lib/semantic/semantic.min.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('lib/semantic/semantic.min.css') }}"></noscript> --}}

    <link href="{{ asset('css/main.css?v=2.2') }}" rel="stylesheet">

    <!-- Favicon -->
	<link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" type="image/png">
	<link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/png">

	<!-- Browser Toolbar Color -->
    <meta name="theme-color" content="#0a4da7">

    <style>
        .fa, .fas, .far, .fal, .fad, .fab {
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            display: inline-block;
            font-style: normal;
            font-variant: normal;
            text-rendering: auto;
            line-height: 1;
            font-display: swap;
        }
        .fa-whatsapp:before {
            content: "\f232";
        }
        #home-slide {
            text-align: center;
            background-color: #0a4da7;
            min-height: calc( 100vh);
            position: relative;
            display: flex;
        }
        body,html {
            height: 100%
        }

        html {
            font-size: 14px
        }

        body {
            margin: 0;
            padding: 0;
            overflow-x: hidden;
            min-width: 320px;
            background: #fff;
            font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;
            font-size: 14px;
            line-height: 1.4285em;
            color: rgba(0,0,0,.87);
            font-smoothing: antialiased
        }

        h1,h2,h3,h4,h5 {
            font-display: swap;
            line-height: 1.28571429em;
            margin: calc(2rem - .14285714em) 0 1rem;
            font-weight: 700;
            padding: 0
        }

        h1 {
            min-height: 1rem;
            font-size: 2rem
        }

        h2 {
            font-size: 1.71428571rem
        }

        h3 {
            font-size: 1.28571429rem
        }
        h1:first-child,h2:first-child,h3:first-child,h4:first-child,h5:first-child {
            margin-top: 0
        }

        h1:last-child,h2:last-child,h3:last-child,h4:last-child,h5:last-child {
            margin-bottom: 0
        }

        p {
            margin: 0 0 1em;
            line-height: 1.4285em
        }

        p:first-child {
            margin-top: 0
        }

        p:last-child {
            margin-bottom: 0
        }

        a {
            color: #4183c4;
            text-decoration: none
        }

        .ui.button {
            cursor: pointer;
            display: inline-block;
            min-height: 1em;
            outline: 0;
            border: none;
            vertical-align: baseline;
            background: #e0e1e2 none;
            color: rgba(0,0,0,.6);
            font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;
            margin: 0 .25em 0 0;
            padding: .78571429em 1.5em .78571429em;
            text-transform: none;
            text-shadow: none;
            font-weight: 700;
            line-height: 1em;
            font-style: normal;
            text-align: center;
            text-decoration: none;
            border-radius: .28571429rem;
            -webkit-box-shadow: 0 0 0 1px transparent inset,0 0 0 0 rgba(34,36,38,.15) inset;
            box-shadow: 0 0 0 1px transparent inset,0 0 0 0 rgba(34,36,38,.15) inset;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-transition: opacity .1s ease,background-color .1s ease,color .1s ease,background .1s ease,-webkit-box-shadow .1s ease;
            transition: opacity .1s ease,background-color .1s ease,color .1s ease,background .1s ease,-webkit-box-shadow .1s ease;
            transition: opacity .1s ease,background-color .1s ease,color .1s ease,box-shadow .1s ease,background .1s ease;
            transition: opacity .1s ease,background-color .1s ease,color .1s ease,box-shadow .1s ease,background .1s ease,-webkit-box-shadow .1s ease;
            will-change: '';
            -webkit-tap-highlight-color: transparent
        }
        .ui.button>.icon:not(.button) {
            height: .85714286em;
            opacity: .8;
            margin: 0 .42857143em 0 -.21428571em;
            -webkit-transition: opacity .1s ease;
            transition: opacity .1s ease;
            vertical-align: '';
            color: ''
        }
        .fa-facebook:before {
            content: "\f09a";
        }
        .fa-instagram:before {
            content: "\f16d";
        }
        .fa-linkedin:before {
            content: "\f08c";
        }
        a#floating {
            position: fixed;
            z-index: 1;
            right: 15px;
            font-size: 40px;
            bottom: -40px;
            transition: bottom 300ms ease-in;
        }
        a#floating.active {
            bottom: 17.5px;
        }
    </style>

	@section('head_styles')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-174006691-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-174006691-1');
    </script>

    <!-- Global site tag (gtag.js) - Google Ads: 696438563 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-696438563"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-696438563'); </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '584664832240233');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=584664832240233&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>

    <header>
        <div class="logo" data-aos="fade-right">
            <!--/inge dejo su compú encendida asi que le dejo este pequeño mensaje y espero que cene bien y que tenga bonita noche y tambien le dice que al inge ivan que decanse y cene chido/
            /con cariño el inge/-->
            <img src="{{ asset('images/PROLUX_Logo_1280x_1_.png') }}" alt="">
        </div>
        {{-- <div>
            <img src="{{ asset('images/cpef.png') }}" alt="" class="home-cpef" style="position:absolute;">
        </div> --}}
        <div class=""  data-aos="fade-up">
            <a href="https://cpef.org.mx/" target="_blank">
                <img src="{{ asset('images/certificacion_prolux_1.png') }}" alt="" style="width:150px; margin: 0 15px; vertical-align: middle;">
            </a>
        </div>
        <div class="phone">
            <a href="https://wa.me/523332313674?text=Hola%2C+me+gustar%C3%ADa+saber+m%C3%A1s+sobre+los+paneles+solares" target="_blank"><i class="fab fa-whatsapp"></i>33 3231 3674</a>
        </div>
        <div class="start">
            <a href="#!" class="button" onclick="openModal()">¡Cotiza ahora!</a>
        </div>
    </header>

    <main>
        @yield('content')
    </main>

    <a href="https://wa.me/523332313674?text=Hola%2C+me+gustar%C3%ADa+saber+m%C3%A1s+sobre+los+paneles+solares" target="_blank" id="floating"><i class="fab fa-whatsapp"></i></a>

    <footer>
        <img src="{{ asset('images/PROLUX_Web_RectanguloAmarillo_520px_.png') }}" alt="" class="sides-squares">
        <div class="logo">
            <img src="{{ asset('images/PROLUX_Logo_1280x_2_.png') }}" alt="">
        </div>
        <p class="address">
            Popocatépetl 1415, Ciudad del Sol<br> Zapopan, Jalisco.
        </p>
        <p class="phone">
            <a href="tel:3331225136">T.(33) 3122 5136</a><br>
            {{-- <a href="https://wa.me/523332313674?text=Hola%2C+me+gustar%C3%ADa+saber+m%C3%A1s+sobre+los+paneles+solares" target="_blank"><i class="fab fa-whatsapp"></i>33 3231 3674</a> --}}
        </p>
        <p class="email">
            <a href="mailto:contacto@prolux.mx">contacto@prolux.mx</a>
        </p>
        <div class="social">
            <a href="https://wa.me/523332313674?text=Hola%2C+me+gustar%C3%ADa+saber+m%C3%A1s+sobre+los+paneles+solares" target="_blank"><i class="fab fa-whatsapp"></i></a>
            <a href="https://www.facebook.com/Prolux.Energy/" target="_blank"><i class="fab fa-facebook"></i></a>
            <a href="https://www.instagram.com/prolux.energy/" target="_blank"><i class="fab fa-instagram"></i></a>
            <a href="https://www.linkedin.com/company/prolux-energy-solutions/" target="_blank"><i class="fab fa-linkedin"></i></a>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="{{ asset('lib/fontawesome/css/fontawesome.css') }}">
    {{-- <link rel="preload" href="{{ asset('lib/fontawesome/css/fontawesome.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('lib/fontawesome/css/fontawesome.css') }}"></noscript> --}}

    <link rel="stylesheet" href="{{ asset('lib/fontawesome/css/brands.css') }}">
    {{-- <link rel="preload" href="{{ asset('lib/fontawesome/css/brands.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('lib/fontawesome/css/brands.css') }}"></noscript> --}}

    <link rel="stylesheet" href="{{ asset('lib/fontawesome/css/solid.css') }}">
    {{-- <link rel="preload" href="{{ asset('lib/fontawesome/css/solid.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('lib/fontawesome/css/solid.css') }}"></noscript> --}}

    {{-- <link rel="stylesheet" href="{{ asset('lib/aos/dist/aos.css?v=1.0.0') }}"> --}}
    <link href="https://unpkg.com/aos@next/dist/aos.css" rel="stylesheet">

    {{-- <link href="{{ asset('css/global.min.css?v=1.0.3') }}" rel="stylesheet"> --}}

    <script src="{{ asset('lib/owl/dist/owl.carousel.min.js') }}"></script>
    {{-- <script src="{{ asset('lib/aos/dist/aos.js?v=1.0.0') }}"></script> --}}
    <script src="{{ asset('lib/semantic/semantic.min.js') }}"></script>
    <!--<script async src="{{ asset('lib/semantic/components/dimmer.min.js') }}"></script>
    <script async src="{{ asset('lib/semantic/components/transition.min.js') }}"></script>
    <script async src="{{ asset('lib/semantic/components/modal.min.js') }}"></script>
    <script async src="{{ asset('lib/semantic/components/popup.min.js') }}"></script>-->
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>

    <!-- cdnjs -->
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>

    <script>
    $(window).scroll(function() {
        // use the value from $(window).scrollTop();
        //console.log('Top '+$(window).scrollTop());
        var scrolled = $(window).scrollTop();
        if (scrolled > 105) {
            $('a#floating').addClass('active');
        } else {
            $('a#floating').removeClass('active');
        }
    });
    </script>
    <script>
        $( document ).ready( function() {
            $("a#floating").on('click', function() {
                console.log("click whatsapp flotante");
                //ga('send', 'event', 'Whatsapp', 'clickFlotante', 'flotante');
                gtag('event', 'clickFlotante', {
                  'event_category': 'Whatsapp',
                  'event_label': 'flotante',
                });
            });
            $("header .phone a").on('click', function() {
                console.log("click whatsapp header");
                //ga('send', 'event', 'Whatsapp', 'clickTelefono', 'header');
                gtag('event', 'clickTelefono', {
                  'event_category': 'Whatsapp',
                  'event_label': 'header',
                });
            });
            $("footer .phone a").on('click', function() {
                console.log("click whatsapp header");
                //ga('send', 'event', 'Whatsapp', 'clickTelefono', 'footer');
                gtag('event', 'clickTelefono', {
                  'event_category': 'Whatsapp',
                  'event_label': 'footer',
                });
            });

        });
    </script>

    @stack('scripts')
    @stack('styles')
</body>
</html>
